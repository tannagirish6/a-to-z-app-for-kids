package com.example.atoz;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    ImageView A,D,X,S,G,M,T,Z,J,K,O,P,H,L,R1;
    MediaPlayer mediaPlayer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        A = findViewById(R.id.A);

        R1 = findViewById(R.id.R1);
        L = findViewById(R.id.L);
        H = findViewById(R.id.H);
        P = findViewById(R.id.P);
        O = findViewById(R.id.O);
        K = findViewById(R.id.K);
        D = findViewById(R.id.D);
        X = findViewById(R.id.X);
        S = findViewById(R.id.S);
        G = findViewById(R.id.G);
        M = findViewById(R.id.M);
        T = findViewById(R.id.T);
        Z = findViewById(R.id.Z);
        J = findViewById(R.id.J);
        K.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setK();
                mediaPlayer.start();
                Toast.makeText(MainActivity.this, "K", Toast.LENGTH_SHORT).show();
            }
        });
        L.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setL();
                mediaPlayer.start();
                Toast.makeText(MainActivity.this, "L", Toast.LENGTH_SHORT).show();
            }
        });
        P.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setP();
                mediaPlayer.start();
                Toast.makeText(MainActivity.this, "P", Toast.LENGTH_SHORT).show();
            }
        });
        O.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setO();
                mediaPlayer.start();
                Toast.makeText(MainActivity.this, "O", Toast.LENGTH_SHORT).show();
            }
        });
        J.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setJ();
                mediaPlayer.start();
                Toast.makeText(MainActivity.this, "J", Toast.LENGTH_SHORT).show();
            }
        });
        A.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setA();
                mediaPlayer.start();
                Toast.makeText(MainActivity.this, "A", Toast.LENGTH_SHORT).show();
            }
        });
        D.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setD();
                mediaPlayer.start();
                Toast.makeText(MainActivity.this, "D", Toast.LENGTH_SHORT).show();
            }
        });
        Z.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setZ();
                mediaPlayer.start();
                Toast.makeText(MainActivity.this, "Z", Toast.LENGTH_SHORT).show();
            }
        });

        X.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setX();
                mediaPlayer.start();
                Toast.makeText(MainActivity.this, "X", Toast.LENGTH_SHORT).show();
            }
        });

        S.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setS();
                mediaPlayer.start();
                Toast.makeText(MainActivity.this, "S", Toast.LENGTH_SHORT).show();
            }
        });
        G.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setG();
                mediaPlayer.start();
                Toast.makeText(MainActivity.this, "G", Toast.LENGTH_SHORT).show();
            }
        });
        M.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setM();
                mediaPlayer.start();
                Toast.makeText(MainActivity.this, "M", Toast.LENGTH_SHORT).show();
            }
        });
        T.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setT();
                mediaPlayer.start();
                Toast.makeText(MainActivity.this, "T", Toast.LENGTH_SHORT).show();
            }
        });
        H.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setH();
                mediaPlayer.start();
                Toast.makeText(MainActivity.this, "H", Toast.LENGTH_SHORT).show();
            }
        });
        R1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setR();
                mediaPlayer.start();
                Toast.makeText(MainActivity.this, "R", Toast.LENGTH_SHORT).show();
            }
        });
    }


    void setD(){
        mediaPlayer = MediaPlayer.create(this,R.raw.d_sound);
    }
    void setA(){
        mediaPlayer = MediaPlayer.create(this,R.raw.a_sound);
    }
    void setX(){
        mediaPlayer = MediaPlayer.create(this,R.raw.x_sound);
    }
    void setS(){
        mediaPlayer = MediaPlayer.create(this,R.raw.s_sound);
    }
    void setR(){
        mediaPlayer = MediaPlayer.create(this,R.raw.r_sound);
    }
    void setL(){
        mediaPlayer = MediaPlayer.create(this,R.raw.l_sound);
    }
    void setO(){
        mediaPlayer = MediaPlayer.create(this,R.raw.o_sound);
    }
    void setG(){
        mediaPlayer = MediaPlayer.create(this,R.raw.g_sound);
    }
    void setJ(){
        mediaPlayer = MediaPlayer.create(this,R.raw.j_sound);
    }
    void setK(){
        mediaPlayer = MediaPlayer.create(this,R.raw.k_sound);
    }
    void setP(){
        mediaPlayer = MediaPlayer.create(this,R.raw.p_sound);
    }
    void setM(){
        mediaPlayer = MediaPlayer.create(this,R.raw.m_sound);
    }
    void setT(){
        mediaPlayer = MediaPlayer.create(this,R.raw.t_sound);
    }
    void setZ(){
        mediaPlayer = MediaPlayer.create(this,R.raw.z_sound);
    }
    void setH(){
        mediaPlayer = MediaPlayer.create(this,R.raw.h_img);
    }
}